#!/usr/bin/env python

from slic.core.acquisition import SFAcquisition
from slic.core.acquisition import PVAcquisition
from slic.core.adjustable import PVAdjustable, DummyAdjustable
from slic.core.condition import PVCondition
from slic.core.scanner import Scanner
from slic.devices.general.delay_stage import DelayStage
from slic.devices.general.motor import Motor
from slic.devices.general.smaract import SmarActAxis
from slic.gui import GUI
from slic.utils import devices
from slic.devices.simpledevice import SimpleDevice
from slic.utils import as_shortcut, Marker

from undulator import Undulators
from undulator import Mono
from undulator import Coupled_MonoUnd

dummy = DummyAdjustable(units="au")

mot_x = Motor("SATES30-RETRO:MOT_X", name="Retro X")
mot_y = Motor("SATES30-RETRO:MOT_Y", name="Retro Y")
mot_z = Motor("SATES30-RETRO:MOT_Z", name="Retro Z")
mot_theta = Motor("SATES30-RETRO:MOT_RY", name="Retro Theta")

retro = SimpleDevice("Retro Stages", x=mot_x, y=mot_y, z=mot_z, theta=mot_theta)

KBV_RX = PVAdjustable("SATOP31-OKBV178:W_RX.VAL", pvname_moving="SATOP31-OKBV178:MOVING", name = "KB_Vertical_RX")
#KBV_RX = PVAdjustable("SATOP31-OKBV178:W_RX.VAL", process_time=1, name = "KBV_RX")
KBH_RY = PVAdjustable("SATOP31-OKBH179:W_RY.VAL", pvname_moving="SATOP31-OKBH179:MOVING", name = "KB_Horiz_RY")
#KBH_RY = PVAdjustable("SATOP31-OKBH179:W_RY.VAL", process_time=1, name = "KBH_RY")

#mono_slits = PVAdjustable("SATOP11-OSGM087:EXITSLIT",pvname_done_moving="SATOP31-OEXS132:MOT_H.DMOV", name = "Mono_Slits" )

n_und_ref = 12
n_unds = [
    6, 7, 8, 9, 10, 11, 12, 13, # 14 is the CHIC
    15, 16, 17, 18, 19, 20, 21, 22
]
chic_fudge_offset = 0
Mon2Unds_offset = -2.

und = Undulators(n_unds, n_und_ref, chic_fudge_offset, name="z Athos Undulators")
#und = Undulators(name="Undulators")

mono_name =  "Athos_mono"
pv_mono_name="SATOP11-OSGM087" 

Mon = Mono(pv_mono_name=pv_mono_name, mono_name=mono_name)
MonUnd = Coupled_MonoUnd(n_unds, n_und_ref, chic_fudge_offset, unds_name="z Athos Undulators", pv_mono_name=pv_mono_name, mono_name=mono_name,  delta=Mon2Unds_offset, name = ("Mono+Und"))


#Mon = PVAdjustable("SATOP11-OSGM087:SetEnergy", pvname_done_moving="SATOP11-OSGM087:MOVING", name="MONO")
laser_delay = DelayStage("SLAAT31-LMOT-M808:MOT", name="Laser Delay")
laser_WP = Motor("SLAAT31-LMOT-M801:MOT", name="Laser WavePlate")

channels = [
    "SATFE10-PEPG046:FCUP-INTENSITY-CAL",
    "SATFE10-PEPG046-EVR0:CALCI",
    "SATFE10-PEPG046:PHOTON-ENERGY-PER-PULSE-AVG",
    "SATES30-LSCP10-FNS:CH0:VAL_GET",
    "SATES30-LSCP10-FNS:CH1:VAL_GET",
    "SATES30-LSCP10-FNS:CH2:VAL_GET",
    "SATES30-LSCP10-FNS:CH4:VAL_GET",
#    "SATES30-LSCP10-FNS:CH0:WFMi"
    "SATES21-CAMS-PATT1:intensity",
    "SATES21-CAMS-PATT1:x_profile",
    "SATES21-CAMS-PATT1:y_profile",
    "SATES21-CAMS-PATT1:FPICTURE",
    "SATES21-CAMS-PATT1:spectrum"

]

pvs = [
    "SATFE10-PEPG046:PHOTON-ENERGY-PER-PULSE-AVG",
    "SATES30-RETRO:MOT_RY.RBV",
    "SATES30-RETRO:MOT_X.RBV",
    "SATES30-RETRO:MOT_Y.RBV",
    "SATES30-RETRO:MOT_Z.RBV"
]

live_channels = [
    "SATES30-LSCP10-FNS:CH0:VAL_GET",
    "SATES30-LSCP10-FNS:CH1:VAL_GET",
#    "SATES30-LSCP10-FNS:CH0:WFM" 
#    "SATES30-LSCP10-FNS:CH4:VAL_GET"
]



instrument = "furka"
pgroup = "p19735"  #Commissioning p group

#check_intensity = PVCondition("SATFE10-PEPG046:FCUP-INTENSITY-CAL", vmin=5, vmax=None, wait_time=3, required_fraction=0.8)
check_intensity = None

daq = SFAcquisition(instrument, pgroup, default_channels=channels, default_pvs=pvs, rate_multiplicator=1)
#daqPV = PVAcquisition(instrument, pgroup, default_channels=live_channels)
scan = Scanner(default_acquisitions=[daq], condition=check_intensity)

gui = GUI(scan, show_goto=True, show_spec=True, show_run=True)


#scanPV = Scanner(default_acquisitions=[daqPV], condition=check_intensity)


'''
Button that runs a function
'''
@as_shortcut
def test():
    print("test")
# use marker() to go to a marker position

'''
Single marker
'''
m1 = Marker(dummy,value=25,name='Normal IN')



print("To start the GUI, run: gui()")



